import scala.collection.immutable.TreeSet

trait Group[T] {
  def zero: T
  def plus(x: T, y: T): T
  def inverse(x: T): T
  def minus(x: T, y: T): T = plus(x, inverse(y))
}

object Group {
  implicit object IntGroup extends Group[Int] {
    val zero: Int = 0
    def plus(x: Int, y: Int): Int = x + y
    def inverse(x: Int): Int = -x
  }
}



case class Product(value: Int, prob: Prob)
case class Prob(choices: Set[Int], sumTarget: Int){
  def evaluate = {
    (0 until choices.size)
      .map{ i =>
        val wiw = choices.dropRight(i)
        val wiwVal = wiw.last
        Product(wiwVal, Prob(wiw, sumTarget - wiwVal))
      }
  }
}

val metaChoices = TreeSet(6, 1, 8)
val metaSumTarget = 15

val hmm = Prob(metaChoices, metaSumTarget)
val result = hmm.evaluate
//// todo turns into this
//(0 until metaChoices.size)
//  .map{ i =>
//    val wiw = metaChoices.dropRight(i)
//    val wiwVal = wiw.last
//    Product(wiwVal, Prob(wiw, metaSumTarget - wiwVal))
//  }

