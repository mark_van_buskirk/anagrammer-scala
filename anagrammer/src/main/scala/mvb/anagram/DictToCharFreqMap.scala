package mvb.anagram

import java.io.File

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import akka.util.ByteString
import mvb.anagram.model.CharFreqMap
import model.CharFreqSubtraction._
import StringFlows._

import scala.concurrent.Future

/**
  * Created by Mark on 2/25/2016.
  */
object DictToCharFreqMap {

  type FreqToCharFreqMap = Map[Int, Map[CharFreqMap, Seq[String]]]

  lazy val makeZeFreqMap = Flow[String]
    .fold(Map.empty: FreqToCharFreqMap) { (acc, str) =>
      val key = CharFreqMap(str)
      val oldInnerMap = acc.getOrElse(key.stringLength, Map.empty[CharFreqMap, Seq[String]])
      val oldInnerMapVal = oldInnerMap.getOrElse(key, Seq.empty[String])
      val newInnerMapVal = oldInnerMapVal :+ str
      val newInnerMap = oldInnerMap.updated(key, newInnerMapVal)
      acc.updated(key.stringLength, newInnerMap)
    }


  def dictCharFreqs(dictFile: File)(implicit actorSystem: ActorSystem): Future[FreqToCharFreqMap] = {
    implicit val materializer = ActorMaterializer()
    fileToLines(dictFile)
      .map(_.filter(_.isLetter))
//      .take(5)
      .via(makeZeFreqMap)
      .runWith(Sink.head)

  }



}
