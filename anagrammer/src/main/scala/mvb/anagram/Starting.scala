package mvb.anagram

import java.io.File

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, IOResult}
import akka.stream.scaladsl._
import akka.util.ByteString
import mvb.anagram.DictToCharFreqMap.FreqToCharFreqMap
import mvb.anagram.model.{Anagramming, AnagramAccumulator, CharFreqMap}
import org.scalactic._
import Snapshots._
import model.CharFreqSubtraction._

import scala.concurrent.Future

/**
  * Created by Mark on 2/20/2016.
  */
object Starting extends App {
//  implicit val actorsystem = ActorSystem("QuickStart")
//
//  import actorsystem.dispatcher
//
//  implicit val materializer = ActorMaterializer()
//  val dictFile = new File("C:\\Users\\Mark\\IdeaProjects\\anagrammer-scala\\src\\main\\resources\\dict.txt")
//  val dictCharFreqMap: Future[FreqToCharFreqMap] = DictToCharFreqMap.dictCharFreqs(dictFile)


//  val inputCharFreqMap = CharFreqMap("hamster")


//  val minWordSizeRequired = 5
//
//  def anagramRequest(s: String) =
//    Source.single(s)
//      .map(CharFreqMap(_))
//      .mapConcat(cf => minWordSizeRequired.to(cf.stringLength).map(n => (n, cf)))

//  def getAnagrams(wordSizeStart: Int, wordRequested: CharFreqMap): Option[Any] = {
//    // If charFreqMap is less than size wordSizeStart
//    dictCharFreqMap.map{ dcfm => dcfm(wordSizeStart).toList.map() }
//
//  }

//  Source.fromFuture(dictCharFreqMap)
//    .mapConcat(m => m(inputCharFreqMap.stringLength - 2).toList)
//    .map { case (dictCharFreq, dictWords) => (freqStrContains(inputCharFreqMap, dictCharFreq), dictWords) }
//    .collect { case (Some(charFreqRemainder), dictWords) => (charFreqRemainder, dictWords) }
//    .runForeach(println)
//    .onComplete { _ => actorsystem.terminate() }

  // make up all the size combinations possible 7, (6,1), (5,2), (5,1,1) etc
  // be able to fulfill any of these with a function
  // Accumulator could have list of lists or tree or something List(List(5),List(List(2), List(1,1))


//  val initialAcc = AnagramAccumulator("hamster")
//  Anagramming.addWords(initialAcc, initialAcc.remainder.stringLength - 3, dictCharFreqMap)
//    .flatMapConcat{ acc =>
//      if(acc.isFinished) {
//        Source.single(acc)
//      } else {
//        Anagramming.addWords(acc, acc.nextWordSize.get, dictCharFreqMap)
//      }
//
//    }
////    .filter{_.isFinished}
//      .take(100)
//    .runForeach(println)
//    .onComplete { _ => actorsystem.terminate() }




  println("hi")


}




