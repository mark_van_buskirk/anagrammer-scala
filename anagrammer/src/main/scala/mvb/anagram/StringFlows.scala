package mvb.anagram

import java.io.File

import akka.stream.scaladsl.{Framing, Flow, FileIO}
import akka.util.ByteString

/**
  * Created by Mark on 2/25/2016.
  */
object StringFlows {


  def fileToLines(file: File) =
    FileIO.fromFile(file)
      .via(byteStringToLines)

  lazy val byteStringToLines = Flow[ByteString]
    .via(Framing.delimiter(
      ByteString("\r\n"),
      maximumFrameLength = 256,
      allowTruncation = true))
    .map(_.utf8String)
}
