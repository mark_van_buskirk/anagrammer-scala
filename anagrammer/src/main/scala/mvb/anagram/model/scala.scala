package mvb.anagram.model

import akka.NotUsed
import akka.stream.scaladsl.Source
import mvb.anagram.DictToCharFreqMap._
import mvb.anagram.model.CharFreqSubtraction._

import scala.concurrent.Future


object CharFreq {
  def toCharFreqTuples(s: String): Seq[(Char, Int)] = {
    val dist: Seq[(Char, Int)] = s.toLowerCase.distinct.sorted
      .map((c: Char) => (c, s.count(_ == c)))
    dist
  }
}

object CharFreqMap {
  def apply(s: String) = new CharFreqMap(CharFreq.toCharFreqTuples(s).toMap)
}

case class CharFreqMap(charFreqs: Map[Char, Int]) {
  val stringLength = charFreqs.values.sum
}

object AnagramAccumulator {
  def apply(s: String) = new AnagramAccumulator(CharFreqMap(s), Seq.empty, None)
}

case class AnagramAccumulator(remainder: CharFreqMap, wordAcc: Seq[Seq[String]], latestAttempt: Option[LatestAttemptDetails] ){
  val nextWordSize = latestAttempt.map{ last =>
    last.wordsExtracted match {
    case true => math.min(last.wordSize, remainder.stringLength)
    case false => math.min(last.wordSize - 1,remainder.stringLength)
  }}

  val isFinished = nextWordSize match {
      case Some(0) => true
      case _ if remainder.stringLength == 0 => true
      case _ => false
    }

}
case class LatestAttemptDetails(wordSize: Int, wordsExtracted: Boolean){
  require(wordSize > 0)
}

trait Anagramming {
//  def addWords(acc: AnagramAccumulator, wordSize: Int)(implicit dictCharFreqMap: FreqToCharFreqMap): AnagramAccumulator
}

object Anagramming extends Anagramming {


  def addWords(acc: AnagramAccumulator, wordSize: Int, dictCharFreqMapFuture: Future[FreqToCharFreqMap]): Source[AnagramAccumulator, NotUsed] = {
    Source.fromFuture(dictCharFreqMapFuture)
      .mapConcat(m => m(wordSize).toList)
      .map { case (dictCharFreq, dictWords) => (freqStrContains(acc.remainder, dictCharFreq), dictWords) }
          .collect { case (Some(charFreqRemainder), dictWords) =>
            AnagramAccumulator(charFreqRemainder, acc.wordAcc :+ dictWords, Some(LatestAttemptDetails(wordSize, dictWords.nonEmpty))) }

  }
}
