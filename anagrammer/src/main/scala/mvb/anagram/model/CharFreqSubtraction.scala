package mvb.anagram.model

/**
  * Created by Mark on 2/25/2016.
  */
object CharFreqSubtraction {

  /**
    *
    * @param big
    * @param small
    * @return Char frequency remainder map, None if does not contain
    */
  def freqStrContains(big: String, small: String): Option[CharFreqMap] =
    freqStrContains(CharFreqMap(big), CharFreqMap(small))

  def freqStrContains(bigCharFreqMap: CharFreqMap, smallCharFreqMap: CharFreqMap): Option[CharFreqMap] = {
    val bs = bigCharFreqMap.charFreqs
    val ss = smallCharFreqMap.charFreqs
    def isPossible = ss.forall { case (char, freq) => bs.get(char).nonEmpty && bs(char) >= freq }
    def remainder = ss.foldLeft(bs) { case (remainderMap, (char, freq)) =>
      remainderMap.updated(char, remainderMap(char) - freq)
    }
    def remainderFiltered = remainder.filter { case (char, freq) => freq > 0 }
    def result = isPossible match {
      case true => Some(CharFreqMap(remainderFiltered))
      case false => None
    }

    result
  }
}
